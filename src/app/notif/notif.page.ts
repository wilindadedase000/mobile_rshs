import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { UserPopoverComponent } from '../user-popover/user-popover.component';
import { ModalController } from '@ionic/angular';
import { NotifContentPage } from '../notif-content/notif-content.page';

@Component({
  selector: 'app-notif',
  templateUrl: './notif.page.html',
  styleUrls: ['./notif.page.scss'],
})
export class NotifPage implements OnInit {

  constructor(public popoverController: PopoverController, public modalController: ModalController) { }
  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: UserPopoverComponent,
      event: ev,
      translucent: true,

    });
    return await popover.present();
  }

  async readmore() {
    const modal = await this.modalController.create({
      component: NotifContentPage,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }

  ngOnInit() {
  }

}
