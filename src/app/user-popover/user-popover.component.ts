import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { AccountSettingsPage } from '../account-settings/account-settings.page';

@Component({
  selector: 'app-user-popover',
  templateUrl: './user-popover.component.html',
  styleUrls: ['./user-popover.component.scss']
})
export class UserPopoverComponent implements OnInit {

  constructor(public viewCtrl: PopoverController, public modalController: ModalController, public navCtrl: NavController) { }
  close() {
    this.viewCtrl.dismiss();
    this.navCtrl.navigateRoot('/login');
  }

  // accountsettings() {
  //   this.viewCtrl.dismiss();
  //   this.navCtrl.navigateRoot('/account-settings');
  // }
  
  async accountsettings() {
    this.viewCtrl.dismiss();
    const modal = await this.modalController.create({
      component: AccountSettingsPage,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }


  ngOnInit() {
  }

}
