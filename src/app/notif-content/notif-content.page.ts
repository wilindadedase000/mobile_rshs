import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-notif-content',
  templateUrl: './notif-content.page.html',
  styleUrls: ['./notif-content.page.scss'],
})
export class NotifContentPage implements OnInit {

  constructor(public modalController: ModalController) { }
  
close() {
  this.modalController.dismiss();
}

  ngOnInit() {
  }

}
