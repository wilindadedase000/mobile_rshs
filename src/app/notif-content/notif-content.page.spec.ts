import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifContentPage } from './notif-content.page';

describe('NotifContentPage', () => {
  let component: NotifContentPage;
  let fixture: ComponentFixture<NotifContentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotifContentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotifContentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
